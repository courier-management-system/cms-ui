const BASE_URL = 'https://clpcrm.com/api/';
export const environment = {
  production: true,
  baseUrl: BASE_URL,
  authServiceURL: BASE_URL + 'auth',
  clientServiceURL: BASE_URL + 'client',
  vehicleServiceURL: BASE_URL + 'train',
  orderMasterURL: BASE_URL + 'order-master',
  paymentURL: BASE_URL + 'payment',
  courierURL: BASE_URL + 'courier',
  expenseURL: BASE_URL + 'expense',
  invoiceURL: BASE_URL + 'invoice'
};
