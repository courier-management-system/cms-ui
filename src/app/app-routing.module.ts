import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppLayoutComponent } from "./layout/app.layout.component";
import { AuthGuard } from './services/auth.guard';

@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: 'dashboard', component: AppLayoutComponent,
                children: [
                    { path: '', loadChildren: () => import('./views/components/dashboard/dashboard.module').then(m => m.DashboardModule) },
                    { path: 'clients', loadChildren: () => import('./views/components/clients/clients.module').then(m => m.ClientsModule) },
                    { path: 'vehicles', loadChildren: () => import('./views/components/train/trains.module').then(m => m.VehiclesModule) },
                    { path: 'orders', loadChildren: () => import('./views/components/orders/orders.module').then(m => m.OrdersModule) },
                    { path: 'payment', loadChildren: () => import('./views/components/payment/payment.module').then(m => m.PaymentModule) },
                    { path: 'expense', loadChildren: () => import('./views/components/expense/expense.module').then(m => m.ExpenseModule) },
                    { path: 'ledger', loadChildren: () => import('./views/components/ledger/ledger.module').then(m => m.LedgerModule) },
                    { path: 'invoice', loadChildren: () => import('./views/components/invoice/invoice.module').then(m => m.InvoiceModule) },
                ],
                canActivate: [AuthGuard]
            },
            {
                path: 'history',
                loadChildren: () => import('./views/components/history/history.module').then(m => m.HistoryModule)
            },
            { path: '', loadChildren: () => import('./views/components/auth/auth.module').then(m => m.AuthModule) },
        ], { scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled', onSameUrlNavigation: 'reload' })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
