export enum ActionMode {
    EDIT = 'edit',
    DELETE = 'delete',
    RESTORE = 'restore',
    PRINT = 'print',
    HISTORY = 'history',
    AGENT_HISTORY = 'agentHistory'
}