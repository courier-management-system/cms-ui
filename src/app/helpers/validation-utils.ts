import { ValidatorFn, AbstractControl, ValidationErrors } from "@angular/forms";

export interface FormErrorMessage {
    key: string;
    arg?: any
}
