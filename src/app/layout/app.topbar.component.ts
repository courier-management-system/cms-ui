import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { TokenStorageService } from '../services/token-storage.service';
import { LayoutService } from "./service/app.layout.service";

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent {

    items!: MenuItem[];

    @ViewChild('menubutton') menuButton!: ElementRef;

    @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

    @ViewChild('topbarmenu') menu!: ElementRef;

    constructor(public layoutService: LayoutService,
        private readonly tokenStorageService: TokenStorageService,
        private readonly router: Router) {
        this.items = [
            {
                label: 'Dashboard',
                icon: 'pi pi-fw pi-home',
                routerLink: ['/dashboard']
            },
            {
                label: 'Clients',
                icon: 'pi pi-fw pi-users',
                routerLink: ['/dashboard/clients']
            },
            {
                label: 'Vehicles',
                icon: 'pi pi-fw pi-car',
                routerLink: ['/dashboard/vehicles']
            },
            {
                label: 'Orders',
                icon: 'pi pi-fw pi-cart-plus',
                items: [
                    {
                        label: 'New order',
                        icon: 'pi pi-fw pi-plus-circle',
                        routerLink: ['/dashboard/orders']
                    },
                    {
                        label: 'Payment',
                        icon: 'pi pi-fw pi-money-bill',
                        routerLink: ['/dashboard/payment']
                    },
                    {
                        label: 'Expenses',
                        icon: 'pi pi-fw pi-circle-off',
                        routerLink: ['/dashboard/expense']
                    }
                ]
            },
            {
                label: 'Reports',
                icon: 'pi pi-fw pi-table',
                items: [
                    {
                        label: 'Ledger',
                        icon: 'pi pi-fw pi-slack',
                        routerLink: ['/dashboard/ledger']
                    }

                ]
            },
            {
                label: 'Quit',
                icon: 'pi pi-fw pi-power-off',
                command: () => this.logout()
            }
        ];

    }

    logout() {
        this.tokenStorageService.signOut();
        this.router.navigate(['/']);
    }
}
