import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppLayoutModule } from './layout/app.layout.module';
import { ClientService } from './views/service/client.service';
import { authInterceptorProviders } from './helpers/auth.interceptor';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        AppRoutingModule,
        AppLayoutModule
    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        ClientService,
        authInterceptorProviders
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
