import { Injectable } from '@angular/core';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class PrintService {

  constructor() { }

  async generatePDF(content: any) {
    await pdfMake.createPdf(content).open();
  }

  generateTableTemplate(cols: any, datas: any, title: string) {
    return [{
      text: title,
      style: 'sectionHeader'
    }, {
      table: {
        headerRows: 1,
        body: [
          [...cols.map((c: any) => c.header)],
          ...datas.map((d: any) => ([...cols.map((c: any) => d[c.field])]))
        ]
      },
      margin: [0, 0, 0, 10]
    }];
  }
}
