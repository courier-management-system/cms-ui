import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const COURIER_URL = environment.courierURL;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class OrderMasterService {

  constructor(private http: HttpClient) { }

  async getOrders() {
    return await this.http.get(COURIER_URL, httpOptions).toPromise();
  }

  async addOrder(data: any) {
    return await this.http.post(COURIER_URL, data, httpOptions).toPromise();
  }

  async updateOrder(data: any, courierId: number) {
    return await this.http.put(`${COURIER_URL}/${courierId}`, data, httpOptions).toPromise();
  }

  async restoreOrder(courierId: number) {
    return await this.http.put(`${COURIER_URL}?id=${courierId}`, httpOptions).toPromise();
  }

  async deleteOrder(courierId: number) {
    return await this.http.delete(`${COURIER_URL}/${courierId}`, httpOptions).toPromise();
  }

}
