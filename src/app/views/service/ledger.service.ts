import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const PAYMENT_URL = environment.paymentURL;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LedgerService {

  constructor(private http: HttpClient) { }

  async getLedgerDetails() {
    return await this.http.get(`${PAYMENT_URL}/ledger`, httpOptions).toPromise();
  }
}
