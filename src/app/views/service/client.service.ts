import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Client } from '../api/interfaces';
import { environment } from 'src/environments/environment';

const CLIENT_SERVICE_URL = environment.clientServiceURL;
const PAYMENT_URL = environment.paymentURL;
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ClientService {

    constructor(private http: HttpClient) { }

    async getClients() {
        return await this.http.get(CLIENT_SERVICE_URL, httpOptions).toPromise();
    }

    async getClientById(clientId: number) {
        return await this.http.get(`${CLIENT_SERVICE_URL}/${clientId}`, httpOptions).toPromise();
    }

    async addClient(data: Client) {
        return await this.http.post(CLIENT_SERVICE_URL, data, httpOptions).toPromise();
    }

    async updateClient(data: Client, clientId: number) {
        return await this.http.put(`${CLIENT_SERVICE_URL}/${clientId}`, data, httpOptions).toPromise();
    }

    async deleteClient(clientId: number) {
        return await this.http.delete(`${CLIENT_SERVICE_URL}/${clientId}`, httpOptions).toPromise();
    }

    async restoreClient(clientId: number) {
        return await this.http.put(`${CLIENT_SERVICE_URL}?id=${clientId}`, httpOptions).toPromise();
    }

    async getPaymentsDetail() {
        return await this.http.get(PAYMENT_URL, httpOptions).toPromise();
    }

    async doPayment(data: any) {
        return await this.http.post(PAYMENT_URL, data, httpOptions).toPromise();
    }

}
