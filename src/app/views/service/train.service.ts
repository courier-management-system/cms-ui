import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Train } from '../api/interfaces';

const SERVICE_URL = environment.vehicleServiceURL;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TrainService {

  constructor(private http: HttpClient) { }

  async getTrains() {
    return await this.http.get(SERVICE_URL, httpOptions).toPromise();
  }

  async addTrain(data: Train) {
    return await this.http.post(SERVICE_URL, data, httpOptions).toPromise();
  }

  async updateTrain(data: Train, trainId: number) {
    return await this.http.put(`${SERVICE_URL}/${trainId}`, data, httpOptions).toPromise();
  }

  async restoreTrain(trainId: number) {
    return await this.http.put(`${SERVICE_URL}?id=${trainId}`, httpOptions).toPromise();
  }

  async deleteTrain(trainId: number) {
    return await this.http.delete(`${SERVICE_URL}/${trainId}`, httpOptions).toPromise();
  }
}
