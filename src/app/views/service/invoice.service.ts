import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const SERVICE_URL = environment.invoiceURL;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(private http: HttpClient) { }

  async getInvoices() {
    return await this.http.get(SERVICE_URL, httpOptions).toPromise();
  }

  async getLastInvoices() {
    return await this.http.get(`${SERVICE_URL}/last-invoice`, httpOptions).toPromise();
  }

  async addInvoice(data: any) {
    return await this.http.post(SERVICE_URL, data, httpOptions).toPromise();
  }

  async updateInvoice(data: any, invoiceId: number) {
    return await this.http.put(`${SERVICE_URL}/${invoiceId}`, data, httpOptions).toPromise();
  }

  async deleteInvoice(invoiceId: number) {
    return await this.http.delete(`${SERVICE_URL}/${invoiceId}`, httpOptions).toPromise();
  }
}
