import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Client } from '../api/interfaces';

const EXPENSE_SERVICE_URL = environment.expenseURL;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  constructor(private http: HttpClient) { }

  async getExpenses() {
    return await this.http.get(EXPENSE_SERVICE_URL, httpOptions).toPromise();
  }

  async addExpense(data: any) {
    return await this.http.post(EXPENSE_SERVICE_URL, data, httpOptions).toPromise();
  }

  async updateExpense(data: any, expenseId: number) {
    return await this.http.put(`${EXPENSE_SERVICE_URL}/${expenseId}`, data, httpOptions).toPromise();
  }

  async restoreExpense(expenseId: number) {
    return await this.http.put(`${EXPENSE_SERVICE_URL}?id=${expenseId}`, httpOptions).toPromise();
  }

  async deleteExpense(expenseId: number) {
    return await this.http.delete(`${EXPENSE_SERVICE_URL}/${expenseId}`, httpOptions).toPromise();
  }
}
