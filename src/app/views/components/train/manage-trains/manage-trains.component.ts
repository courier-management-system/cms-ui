import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { ActionMode } from 'src/app/helpers/menus';
import { FormErrorMessage } from 'src/app/helpers/validation-utils';
import { Train } from 'src/app/views/api/interfaces';
import { TrainService } from 'src/app/views/service/train.service';

@Component({
  selector: 'app-manage-trains',
  templateUrl: './manage-trains.component.html',
  styleUrls: ['./manage-trains.component.scss']
})
export class ManageTrainsComponent implements OnInit {
  trainDialog: boolean = false;
  trainsDetailCopy: any[] = [];
  deleteTrainDialog: boolean = false;
  deleteTrainsDialog: boolean = false;
  trains: Train[] = [];
  train: Train = {};
  selectedTrains = [];
  submitted: boolean = false;
  cols: any[] = [];
  rowsPerPageOptions = [5, 10, 20];
  form: FormGroup;
  isDeletedTab = false;
  actionMenu: any[] = [{ id: 'edit', label: 'Edit' }, { id: 'delete', label: 'Delete' }];
  errors: { [key: string]: FormErrorMessage } = {};

  constructor(private messageService: MessageService,
    private trainService: TrainService,
    private fb: FormBuilder) {
    this.form = fb.group({
      trainNumber: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]*$/)]),
      toStation: new FormControl('', [Validators.pattern(/^(\w+ ?)*$/)]),
      fromStation: new FormControl('', [Validators.pattern(/^(\w+ ?)*$/)])
    }, { updateOn: 'blur' });
    this.collectErrorOnValueChanges();

  }

  ngOnInit() {
    this.fetchData();
    this.cols = [
      { field: 'trainNumber', header: 'Number' },
      { field: 'toStation', header: 'To Station' },
      { field: 'fromStation', header: 'From Station' }
    ];

  }

  collectErrorOnValueChanges() {
    Object.keys(this.form.controls).forEach((key: string) => {
      this.form.get(key)?.valueChanges.subscribe(() => {
        delete this.errors[key];
        const controlErrors: any = this.form.get(key)?.errors || null;
        if (controlErrors !== null) {
          if (key === 'trainNumber' && controlErrors?.pattern) {
            this.errors[key] = { key, arg: 'Invalid train number' };
          } else if (key === 'toStation' && controlErrors?.pattern) {
            this.errors[key] = { key, arg: 'Station name is invalid' };
          } else if (key === 'fromStation' && controlErrors.pattern) {
            this.errors[key] = { key, arg: 'Station name is invalid' };
          }
        }
      });
    });
  }

  async fetchData() {
    try {
      let result: any = await this.trainService.getTrains();
      if (result?.statusCode === 200) {
        this.trainsDetailCopy = result?.data;
        this.filterTrains();
      }
    } catch (error) {
      console.log('error occured while fetching client', error);
    }
  }

  filterTrains() {
    if (this.trainsDetailCopy.length) {
      if (this.isDeletedTab) {
        this.trains = this.trainsDetailCopy?.filter((expense: any) => expense.isDeleted);
      } else {
        this.trains = this.trainsDetailCopy?.filter((expense: any) => !expense.isDeleted);
      }
    }
  }

  openNew() {
    this.train = {};
    this.submitted = false;
    this.trainDialog = true;
  }

  deleteSelectedTrains() {
    this.deleteTrainsDialog = true;
  }

  editTrain(train: Train) {
    this.train = { ...train };
    let frmCtl: any = this.form.controls;
    frmCtl.trainNumber.setValue(train.trainNumber);
    frmCtl.toStation.setValue(train.toStation);
    frmCtl.fromStation.setValue(train.fromStation);
    this.trainDialog = true;
  }

  deleteTrain(train: Train) {
    this.deleteTrainDialog = true;
    this.train = { ...train };
  }

  confirmDeleteSelected() {
    this.deleteTrainsDialog = false;
  }

  async confirmDelete() {
    this.deleteTrainDialog = false;
    try {
      let trainId = this.train['id'];
      if (trainId) {
        await this.trainService.deleteTrain(trainId);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Train Deleted', life: 3000 });
        this.fetchData();
      }
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'System error occured', life: 3000 });
      console.log('error occurred while deleting train', error);
    }
    this.train = {};
  }

  hideDialog() {
    this.trainDialog = false;
    this.submitted = false;
    this.form.reset();
    this.train = {};
  }

  handleTabChange(evt: any) {
    var index = evt.index;
    this.isDeletedTab = index === 1;
    if (this.isDeletedTab) {
      this.actionMenu = [{ id: 'restore', label: 'Restore' }];
    } else {
      this.actionMenu = [{ id: 'edit', label: 'Edit' }, { id: 'delete', label: 'Delete' }];
    }
    this.filterTrains();
  }

  async saveTrain() {
    this.submitted = true;
    if (this.form.valid) {
      let payload = this.form.getRawValue();
      if (this.train.id) {
        try {
          await this.trainService.updateTrain(payload, this.train.id);
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Train Updated', life: 3000 });
          this.fetchData();
          this.train = {};
          this.hideDialog();
        } catch (error) {
          console.log('Error occured while saving train details', error);
          this.messageService.add({ severity: 'error', summary: 'Unsuccessful', detail: 'System error occured', life: 3000 });
        }
      } else {
        try {
          await this.trainService.addTrain(payload);
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Train Added', life: 3000 });
          this.fetchData();
          this.train = {};
          this.hideDialog();
        } catch (error) {
          console.log('Error occured while saving train details', error);
          this.messageService.add({ severity: 'error', summary: 'Unsuccessful', detail: 'System error occured', life: 3000 });
        }
      }
    }
  }

  async restoreTrain(train: any) {
    try {
      await this.trainService.restoreTrain(train.id);
      this.fetchData();
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Train restored', life: 3000 });
    } catch (e) {
      console.log('error occured while restoring expense details', e);

    }
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }

  selectActionMenu(evt: any) {
    if (evt.action === ActionMode.EDIT.toString()) {
      this.editTrain(evt.data);
    } else if (evt.action === ActionMode.DELETE.toString()) {
      this.deleteTrain(evt.data);
    } else if (evt.action === ActionMode.RESTORE.toString()) {
      this.restoreTrain(evt.data);
    }
  }

}
