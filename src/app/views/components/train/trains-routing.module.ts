import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageTrainsComponent } from './manage-trains/manage-trains.component';

const routes: Routes = [{ path: '', component: ManageTrainsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesRoutingModule { }
