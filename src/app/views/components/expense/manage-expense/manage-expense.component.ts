import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { ExpenseService } from 'src/app/views/service/expense.service';
// @ts-ignore
import { DateTime } from 'luxon';
import { ActionMode } from 'src/app/helpers/menus';
import { FormErrorMessage } from 'src/app/helpers/validation-utils';
@Component({
  selector: 'app-manage-expense',
  templateUrl: './manage-expense.component.html',
  styleUrls: ['./manage-expense.component.scss']
})
export class ManageExpenseComponent implements OnInit {

  expensesDetailCopy: any[] = [];
  expenseDialog: boolean = false;
  deleteExpenseDialog: boolean = false;
  deleteExpensesDialog: boolean = false;
  expenses: any[] = [];
  expense: any = {};
  selectedExpenses = [];
  submitted: boolean = false;
  actionMenu: any[] = [{ id: 'edit', label: 'Edit' }, { id: 'delete', label: 'Delete' }];
  cols: any[] = [];
  rowsPerPageOptions = [5, 10, 20];
  form: FormGroup;
  expenseDate = new Date;
  selectedExpenseId = null;
  isDeletedTab = false;
  errors: { [key: string]: FormErrorMessage } = {};

  constructor(private messageService: MessageService,
    private expenseService: ExpenseService,
    private fb: FormBuilder) {
    this.form = fb.group({
      description: new FormControl(''),
      amount: new FormControl('', [Validators.required, this.rateValidator()]),
      type: new FormControl('', [Validators.pattern(/^[a-zA-Z ]*$/)])
    });
    this.collectErrorOnValueChanges();
  }

  ngOnInit() {
    this.fetchData();
    this.cols = [
      { field: 'type', header: 'Type' },
      { field: 'date', header: 'Date' },
      { field: 'amount', header: 'Amount' },
      { field: 'description', header: 'DEscription' }
    ];
  }

  collectErrorOnValueChanges() {
    Object.keys(this.form.controls).forEach((key: string) => {
      this.form.get(key)?.valueChanges.subscribe(() => {
        delete this.errors[key];
        const controlErrors: any = this.form.get(key)?.errors || null;
        if (controlErrors !== null) {
          if (key === 'type' && controlErrors?.pattern) {
            this.errors[key] = { key, arg: 'Type should contains alphabet only' };
          } else if (key === 'amount' && controlErrors?.invalid) {
            this.errors[key] = { key, arg: 'Amount cannot be less than one' };
          }
        }
      });
    });
  }

  async fetchData() {
    try {
      let result: any = await this.expenseService.getExpenses();
      if (result?.statusCode === 200) {
        this.expensesDetailCopy = result?.data;
        this.filterExpense();
      }
    } catch (error) {
      console.log('error occured while fetching client', error);
    }
  }

  filterExpense() {
    if (this.expensesDetailCopy.length) {
      if (this.isDeletedTab) {
        this.expenses = this.expensesDetailCopy?.filter((expense: any) => expense.isDeleted);
      } else {
        this.expenses = this.expensesDetailCopy?.filter((expense: any) => !expense.isDeleted);
      }
    }
  }

  openNew() {
    this.expense = {};
    this.submitted = false;
    this.expenseDialog = true;
  }

  deleteSelectedExpenses() {
    this.deleteExpensesDialog = true;
  }

  editExpense(expense: any) {
    this.expense = { ...expense };
    let frmCtl: any = this.form.controls;
    frmCtl.amount.setValue(expense.amount);
    frmCtl.description.setValue(expense.description);
    frmCtl.type.setValue(expense.type);
    this.expenseDate = new Date(expense.date);
    this.expenseDialog = true;
  }

  deleteExpense(expense: any) {
    this.deleteExpenseDialog = true;
    this.expense = { ...expense };
  }

  confirmDeleteSelected() {
    this.deleteExpensesDialog = false;
  }

  async confirmDelete() {
    this.deleteExpenseDialog = false;
    try {
      let expenseId = this.expense['id'];
      if (expenseId) {
        await this.expenseService.deleteExpense(expenseId);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Expense Deleted', life: 3000 });
        this.fetchData();
      }
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'System error occured', life: 3000 });
      console.log('error occurred while deleting expense', error);
    }
    this.expense = {};
  }

  hideDialog() {
    this.expenseDialog = false;
    this.submitted = false;
    this.form.reset();
    this.expense = {};
    this.expenseDate = new Date();
  }

  handleTabChange(evt: any) {
    var index = evt.index;
    this.isDeletedTab = index === 1;
    if (this.isDeletedTab) {
      this.actionMenu = [{ id: 'restore', label: 'Restore' }];
    } else {
      this.actionMenu = [{ id: 'edit', label: 'Edit' }, { id: 'delete', label: 'Delete' }];
    }
    this.filterExpense();
  }

  async saveExpense() {
    this.submitted = true;
    if (this.form.valid) {
      let payload = this.form.getRawValue();
      payload['date'] = DateTime.fromJSDate(this.expenseDate).toFormat('yyyy-MM-dd');
      if (this.expense.id) {
        try {
          await this.expenseService.updateExpense(payload, this.expense.id);
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Expense Updated', life: 3000 });
          this.fetchData();
          this.expense = {};
          this.hideDialog();
        } catch (error) {
          console.log('Error occured while saving expense details', error);
          this.messageService.add({ severity: 'error', summary: 'Unsuccessful', detail: 'System error occured', life: 3000 });
        }
      } else {
        try {
          await this.expenseService.addExpense(payload);
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Expense Added', life: 3000 });
          this.fetchData();
          this.expense = {};
          this.hideDialog();
        } catch (error) {
          console.log('Error occured while saving expense details', error);
          this.messageService.add({ severity: 'error', summary: 'Unsuccessful', detail: 'System error occured', life: 3000 });
        }
      }
    }
  }

  async restoreExpense(expense: any) {
    try {
      await this.expenseService.restoreExpense(expense.id);
      this.fetchData();
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Expense restored', life: 3000 });
    } catch (e) {
      console.log('error occured while restoring expense details', e);

    }
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }

  selectActionMenu(evt: any) {
    if (evt.action === ActionMode.EDIT.toString()) {
      this.editExpense(evt.data);
    } else if (evt.action === ActionMode.DELETE.toString()) {
      this.deleteExpense(evt.data);
    } else if (evt.action === ActionMode.RESTORE.toString()) {
      this.restoreExpense(evt.data);
    }
  }

  rateValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;
      return value < 0 ? { invalid: true } : null;
    }
  }

}
