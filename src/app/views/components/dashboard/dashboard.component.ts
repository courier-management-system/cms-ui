import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../service/client.service';
import { OrderMasterService } from '../../service/order-master.service';
import { ExpenseService } from '../../service/expense.service';
// @ts-ignore
import { DateTime } from 'luxon';

@Component({
    templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

    items: any[] = [];
    data: any;
    lineChartData: any;
    orders: any;
    expense: any;
    basicOptions: any;

    constructor(private clientService: ClientService,
        private orderService: OrderMasterService,
        private expenseService: ExpenseService
    ) {
    }

    async ngOnInit() {
        try {
            let client: any = await this.clientService.getClients();
            this.items.push({
                title: 'Total Client', value: client.data?.length, icon: 'pi-users'
            });
            let totalOutstanding = 0;
            this.orders = await this.orderService.getOrders();
            this.items.push({
                title: 'Total Orders', value: this.orders.data?.length, icon: 'pi-shopping-cart'
            });
            this.orders.data?.forEach((d: any) => totalOutstanding += d.balanceAmount);
            this.items.push({
                title: 'Total Outstanding', value: totalOutstanding, icon: 'pi-money-bill'
            });
            this.expense = await this.expenseService.getExpenses();
            let currentMonthYear = DateTime.now().toFormat('MM/yyyy');
            let totalExpense = 0;
            this.expense.data?.filter((item: any) => DateTime.fromISO(item.date).toFormat('MM/yyyy') === currentMonthYear).forEach((item: any) => totalExpense += item.amount);
            this.items.push({
                title: 'Total Expense', value: totalExpense, icon: 'pi-money-bill'
            });

            let allExpense = 0;
            this.expense.data?.forEach((item: any) => allExpense += item.amount);
            let totalPaidAmount = 0;
            this.orders.data?.forEach((d: any) => totalPaidAmount += d.balanceAmount);
            this.data = {
                labels: ['Outstanding', 'Paid', 'Expense'],
                datasets: [
                    {
                        data: [totalOutstanding, totalPaidAmount, allExpense],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }
                ]
            };

            this.generateLineChartData();
        } catch (err) {
            console.log('error occured while fetching data', err);
        }
    }

    generateLineChartData() {
        const d = new Date();
        let currentYear = d.getFullYear();
        let currentMonth = d.getMonth() + 1;
        let outstandingData = [];
        let pendingData = [];
        this.orders.data = this.orders.data?.filter((item: any) => DateTime.fromISO(item.courierDate).toFormat('yyyy') === currentYear);
        // for(let i = 0; i <= currentMonth; i++) {
        //     let month = i < 9 ? `0{i}` : i;
        //     let data = this.orders.data?.filter((data: any) => DateTime.fromISO(data.courierDate).toFormat('MM/yyyy') === `${month}/${currentYear}`);
        //     let currentMonthOutstanding = data.forEach
        //     outstandingData.push(data);
        // }
        this.lineChartData = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [
                {
                    label: 'Outstanding',
                    data: [65, 59, 80, 81, 56, 55, 40],
                    fill: false,
                    borderColor: "#FF6384",
                    tension: .4
                },
                {
                    label: 'Paid',
                    data: [28, 40, 30, 19, 86, 87, 90],
                    fill: false,
                    borderColor: "#36A2EB",
                    tension: .4
                },
                {
                    label: 'Expense',
                    data: [28, 48, 40, 19, 86, 27, 90],
                    fill: false,
                    borderColor: '#FFA726',
                    tension: .4
                }
            ]
        };

        this.basicOptions = {
            plugins: {
                legend: {
                    labels: {
                        color: '#495057'
                    }
                }
            },
            scales: {
                x: {
                    ticks: {
                        color: '#495057'
                    },
                    grid: {
                        color: '#ebedef'
                    }
                },
                y: {
                    ticks: {
                        color: '#495057'
                    },
                    grid: {
                        color: '#ebedef'
                    }
                }
            }
        };
    }
}
