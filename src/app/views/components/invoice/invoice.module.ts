import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceRoutingModule } from './invoice-routing.module';
import { TableComponent } from './table/table.component';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { OrdersModule } from '../orders/orders.module';
import { PrintTemplateComponent } from './print-template/print-template.component';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { TableMenuModule } from '../table-menu/table-menu.module';
import { ProgressBarModule } from 'primeng/progressbar';

@NgModule({
  declarations: [
    TableComponent,
    PrintTemplateComponent
  ],
  imports: [
    CommonModule,
    InvoiceRoutingModule,
    OrdersModule,
    TableModule,
    InputTextModule,
    ButtonModule,
    DialogModule,
    ToastModule,
    TableMenuModule,
    ProgressBarModule 
  ],
  providers: [MessageService]
})
export class InvoiceModule { }
