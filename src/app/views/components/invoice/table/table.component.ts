import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { InvoiceService } from 'src/app/views/service/invoice.service';
import { PrintService } from '../../../service/print.service'

import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
import { MessageService } from 'primeng/api';
import { ActionMode } from 'src/app/helpers/menus';
(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;
import { NumToWordConverterService } from 'src/app/views/service/num-to-word-converter.service';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  cols: any[] = [];
  tableData: any[] = [];
  invoiceDialog = false;
  invoiceData: any[] = [];
  actionMenu = [{ id: 'edit', label: 'Edit' }, { id: 'delete', label: 'Delete' }, { id: 'print', label: 'Print' }];
  @ViewChild('printTemplate') content: ElementRef;
  selectedInvoice: any;
  todayDate = new Date();
  loadPrintData = false;

  constructor(private readonly invoiceService: InvoiceService,
    private readonly messageService: MessageService,
    private readonly numToWordService: NumToWordConverterService
  ) { }

  ngOnInit(): void {
    this.fetchData();
    this.cols = [
      { field: 'invoiceNumber', header: 'Invoice Number' },
      { field: 'vphNumber', header: 'VPH Numbers' },
      { field: 'totalAmount', header: 'totalAmount' }
    ];
  }

  async fetchData() {
    try {
      let result: any = await this.invoiceService.getInvoices();
      if (result?.statusCode === 200) {
        this.tableData = result?.data;
        this.tableData.reverse();
      }
    } catch (error) {
      console.log('error occured while fetching invoices', error);
    }
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }

  getAllVPHNumbers(items: any) {
    let vphNumers: any[] = [];
    items?.forEach((i: any) => {
      vphNumers.push(i.vphNumber);
    });
    return vphNumers.join(', ');
  }

  openDialog(selectedData: any) {
    this.invoiceDialog = true;
    this.invoiceData = [selectedData];
  }

  hideInvoiceDialog() {
    setTimeout(() => this.fetchData(), 1000);
    this.invoiceDialog = false;
  }

  async printInvoice() {
    let popupWin = window.open('', '_blank');
    popupWin?.document
      .write(`<html>
          <body onload="window.print();">${this.content.nativeElement.innerHTML}</body></html>`);
    popupWin?.document.write(`<style>
    @page 
      {
        size: landscape;
        margin: 5mm;
        footer {page-break-after: always;}
      }
      .items-table {
        text-align: center;
        margin-top: 20px;
      }
      table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 7px;
      }
      
      th, td {
        width:auto;
        min-width: 138px;
      }

      table {
        width: 100%;
      }
  
      .overme {
        width: 125px;
        overflow:hidden; 
        white-space:nowrap; 
        text-overflow: ellipsis;
        color: black;
      }

      .uppercase {
        text-transform: uppercase;
      }
  
      .detail-table {
        text-align: left;
        .heading {
          display: flex;
          flex-direction: column;
          #tax-invoice {
            text-align: center;
          }
          #act {
            text-align: center;
          }
        }

        .company-name {
          width: 100%;
          text-align: center;
          h2 {
            font-weight: 800;
            font-family: ui-monospace;
          }
        }
      }

      .center {
        text-align: center;
      }
      </style>
    `);
    popupWin?.document.close();
    this.loadPrintData = false;

  }

  async deleteInvoice(invoiceId: number) {
    try {
      await this.invoiceService.deleteInvoice(invoiceId);
      setTimeout(() => this.fetchData(), 1000);
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Invoice Deleted', life: 3000 });
    } catch (err) {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'System error occured', life: 3000 });
    }
  }

  selectActionMenu(evt: any) {
    if (evt.action === ActionMode.EDIT.toString()) {
      this.openDialog(evt.data);
    } else if (evt.action === ActionMode.DELETE.toString()) {
      this.deleteInvoice(evt.data?.id);
    } else if (evt.action === ActionMode.PRINT.toString()) {
      this.loadPrintData = true;
      this.selectedInvoice = evt.data;
      setTimeout(() => this.printInvoice(), 2000);
    }
  }

  convertToWord(amount: any) {
    return this.numToWordService.currencyConverter(amount);
  }

}
