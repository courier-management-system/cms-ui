import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { ClientService } from 'src/app/views/service/client.service';
import { OrderMasterService } from 'src/app/views/service/order-master.service';
// @ts-ignore
import { DateTime } from 'luxon';


@Component({
  selector: 'app-manage-payment',
  templateUrl: './manage-payment.component.html',
  styleUrls: ['./manage-payment.component.scss']
})
export class ManagePaymentComponent implements OnInit {

  clients: any[] = [];
  payments: any[] = [];
  paymentDialog = false;
  courierPaymentDetail: any[] = [];
  remainingPaymentCourierDetail = [];
  selectedcourierDetail: any[] = [];
  selectedClient: any;
  filteredClient: any[] = [];
  cols: any[] = [];
  amountToPay: string | number;
  paymentDate = new Date;
  remark = '';

  constructor(private readonly clientService: ClientService,
    private readonly courierService: OrderMasterService,
    private messageService: MessageService,
  ) {

    this.cols = [
      { field: 'clientName', header: 'VPH Number' },
      { field: 'totalAmount', header: 'Total Amount' },
      { field: 'paidAmount', header: 'Paid Amount' },
      { field: 'balanceAmount', header: 'Balance Amount' },
    ];
  }

  async ngOnInit() {
    let client: any = await this.clientService.getClients();
    if (client.statusCode === 200) {
      this.clients = client.data;
    }
    this.getPaymentDetail();
  }

  async getPaymentDetail() {
    let payment: any = await this.clientService.getPaymentsDetail();
    if (payment.statusCode === 200) {
      this.payments = payment.data;
    }
  }

  openPaymentDialog() {
    this.paymentDialog = true;
  }

  onSelectClient(evt: any) {
    if (this.payments.length && this.selectedClient?.id) {
      let unPaidDetail = this.payments.find(payment => payment.clientId === this.selectedClient?.id);
      this.selectedClient['payment'] = unPaidDetail;
    }
  }

  filterClient(event: any) {
    let filtered: any[] = [];
    let query = event.query;

    for (let i = 0; i < this.clients.length; i++) {
      let client = this.clients[i];
      if (client.companyName.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(client);
      }
    }
    this.filteredClient = filtered;
  }

  hideDialog() {
    this.selectedClient = null;
    this.filteredClient = [];
    this.paymentDialog = false;
    this.amountToPay = '';
    this.remark = '';
    this.paymentDate = new Date;
  }

  async savePayment() {
    if (Number(this.amountToPay) > 0) {
      try {
        let payload = {
          clientId: this.selectedClient?.id,
          amountToPay: Number(this.amountToPay),
          paymentDate: DateTime.fromJSDate(this.paymentDate).toFormat('yyyy-MM-dd'),
          remark: this.remark
        };
        await this.clientService.doPayment(payload);
        setTimeout(() => this.getPaymentDetail(), 1000);
        this.hideDialog();
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Payment saved successfully', life: 3000 });
      } catch (error) {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'System error occured', life: 3000 });
        console.log('error occured while saving payment', error);
      }
    }
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }

}
