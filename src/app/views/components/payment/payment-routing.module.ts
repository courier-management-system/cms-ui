import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagePaymentComponent } from './manage-payment/manage-payment.component';

const routes: Routes = [{ path: '', component: ManagePaymentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
