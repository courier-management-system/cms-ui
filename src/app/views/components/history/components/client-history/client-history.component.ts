import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientService } from 'src/app/views/service/client.service';
import { LedgerService } from 'src/app/views/service/ledger.service';
import { OrderMasterService } from 'src/app/views/service/order-master.service';
// @ts-ignore
import { DateTime } from 'luxon';
import { PrintService } from 'src/app/views/service/print.service';

@Component({
  selector: 'app-client-history',
  templateUrl: './client-history.component.html',
  styleUrls: ['./client-history.component.scss']
})
export class ClientHistoryComponent implements OnInit {

  orderTitle = 'Orders Detail';
  paymentTitle = 'Payments Detail';
  clientId: number;
  orders: any[] = [];
  payments: any[] = [];
  cardDetails: any[] = [];
  clientName = '';
  orderCols: any[] = [{ field: 'vphNumber', header: 'VPH Number' }, { field: 'trainNumber', header: 'Train Number' },
  { field: 'packetNumber', header: 'Packet Number' }, { field: 'weight', header: 'Weight' },
  { field: 'rate', header: 'Rate' }, { field: 'totalAmount', header: 'Total Amount' },
  { field: 'paidAmount', header: 'Paid Amount' }];

  paymentCols: any[] = [{ field: 'paidAmount', header: 'Paid Amount' }, { field: 'remark', header: 'Remark' },
  { field: 'date', header: 'Date', filter: { isFilter: true } }];
  orderPrintData: any[] = [];
  paymentPrintData: any[] = [];
  constructor(
    private clientService: ClientService,
    private route: ActivatedRoute,
    private orderService: OrderMasterService,
    private ledgerService: LedgerService,
    private printService: PrintService
  ) { }

  async ngOnInit() {
    this.clientId = this.route.snapshot.params['id'];
    try {
      let response: any = await this.clientService.getClientById(this.clientId);      
      if (response.statusCode === 200) {
        this.clientName = response.data.companyName.toUpperCase();
      }
    } catch(err) {
      console.log('error occured while fetching client details', err);
    }
    await this.getOrdersDetail();
    await this.getPaymentsDetail();
    await this.createCardDetails();
  }

  async getOrdersDetail() {
    try {
      let response: any = await this.orderService.getOrders();
      if (response.statusCode === 200) {
        this.orders = response.data.filter((item: any) => item.clientId === Number(this.clientId)).map((item: any) => {
          return {
            vphNumber: item.vphNumber, trainNumber: item.train?.trainNumber,
            packetNumber: item.packetNumber, weight: item.weight, rate: item.rate,
            totalAmount: item.totalAmount, paidAmount: item.paidAmount,
          };
        });
      }
    } catch (err) {
      console.log('error occured while fetching order details', err);
    }
  }

  async getPaymentsDetail() {
    try {
      let response: any = await this.ledgerService.getLedgerDetails();
      if (response.statusCode === 200) {
        this.createPaymentCompatibleData(response.data);
      }
    } catch (err) {
      console.log('error occured while fetching order details', err);
    }
  }

  createPaymentCompatibleData(data: any) {
    data = data.filter((d: any) => d.client.id === Number(this.clientId))[0];
    this.payments = data?.paymentHistory?.filter((item: any) => item.creditAmount > 0).map((payment: any) => {
      return {
        paidAmount: payment.creditAmount,
        remark: payment.remark,
        date: DateTime.fromISO(payment.paymentDate).toFormat('dd-MM-yyyy')
      };
    });
  }

  createCardDetails() {
    let totalAmount = 0;
    let totalPaidAmount = 0;
    this.orders.forEach(order => totalAmount += order.totalAmount);
    this.payments.forEach(payment => totalPaidAmount += payment.paidAmount);
    this.cardDetails.push({ title: 'Total Orders', value: this.orders?.length });
    this.cardDetails.push({ title: 'Total Amount', value: totalAmount });
    this.cardDetails.push({ title: 'Total Paid Amount', value: totalPaidAmount });
  }

  orderPrint(data: any) {
    this.orderPrintData = data;    
  }

  paymentPrint(data: any) {
    this.paymentPrintData = data;
  }

  async print() {
    let ordersTable = this.printService.generateTableTemplate(this.orderCols, this.orderPrintData, 'Orders Detail');
    let paymentTable = this.printService.generateTableTemplate(this.paymentCols, this.paymentPrintData, 'Payment Detail');

    let totalAmount = 0;
    let totalPaidAmount = 0;
    this.orderPrintData.forEach(order => totalAmount += order.totalAmount);
    this.paymentPrintData.forEach(payment => totalPaidAmount += payment.paidAmount);
    
    let docDefinition = {
      content: [
        {
          columns: [
            [
              {
                text: `Client Name: ${this.clientName}`,
                bold: true,
                margin: [0, 0, 0, 10]
              },
              {
                text: `Total Orders: ${this.orderPrintData?.length}`,
                bold: true,
                margin: [0, 0, 0, 10]
              },
            ],
            [
              {
                text: `Total Paid Amount: ${totalPaidAmount}`,
                bold: true,
                margin: [0, 0, 0, 10]
              },
              {
                text: `Total Amount: ${totalAmount}`,
                bold: true,
                margin: [0, 0, 0, 10]
              },
            ]
          ]
        }, 
        ordersTable, paymentTable
      ],
      styles: {
        sectionHeader: {
          bold: true,
          fontSize: 14,
          margin: [0, 5, 0, 5]
        }
      }
    }; 
    await this.printService.generatePDF(docDefinition);
  }
}
