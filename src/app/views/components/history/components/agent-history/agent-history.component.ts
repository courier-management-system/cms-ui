import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderMasterService } from 'src/app/views/service/order-master.service';
// @ts-ignore
import { DateTime } from 'luxon';
import { PrintService } from 'src/app/views/service/print.service';

@Component({
  selector: 'app-agent-history',
  templateUrl: './agent-history.component.html',
  styleUrls: ['./agent-history.component.scss']
})
export class AgentHistoryComponent implements OnInit {

  agentName = '';
  clientId: number;
  orders: any[] = [];
  cols: any[] = [];
  title = 'Details';
  orderPrintData: any[] = [];
  constructor(
    private route: ActivatedRoute,
    private orderService: OrderMasterService,
    private printService: PrintService
  ) { }

  async ngOnInit() {
    this.cols = [
      { field: 'vphNumber', header: 'VPH Number' },
      { field: 'clientName', header: 'Client Name' },
      { field: 'trainNumber', header: 'Train Number' },
      { field: 'packetNumber', header: 'Packet Number' },
      { field: 'weight', header: 'Weight' },
      { field: 'rate', header: 'Rate' },
      { field: 'totalAmount', header: 'Total Amount' },
      { field: 'date', header: 'Order Date', filter: { isFilter: true } }
    ];
    this.agentName = this.route.snapshot.params['id'].replaceAll('%', ' ');
    await this.getOrders();
  }

  async getOrders() {
    try {
      let result: any = await this.orderService.getOrders();
      if (result?.statusCode === 200) {
        this.orders = result.data.filter((data: any) => data.client?.agentName === this.agentName).map((item: any) => {
          return {
            vphNumber: item.vphNumber, clientName: item.client?.companyName.toUpperCase(),
            trainNumber: item.train?.trainNumber, packetNumber: item.packetNumber,
            weight: item.weight, rate: item.rate, totalAmount: item.totalAmount,
            date: DateTime.fromISO(item.courierDate).toFormat('dd-MM-yyyy')
          }
        });
      }
    } catch (error) {
      console.log('error occured while fetching order', error);
    }
  }

  orderPrint(data: any) {
    this.orderPrintData = data;
  }

  async print() {
    let ordersTable = this.printService.generateTableTemplate(this.cols, this.orderPrintData, 'Orders Detail');

    let docDefinition = {
      content: [
        {
          columns: [
            [
              {
                text: `Name: ${this.agentName}`,
                bold: true,
                margin: [0, 0, 0, 10]
              },
            ],
          ]
        },
        ordersTable
      ],
      styles: {
        sectionHeader: {
          bold: true,
          fontSize: 14,
          margin: [0, 5, 0, 5]
        }
      }
    };
    await this.printService.generatePDF(docDefinition);
  }
}
