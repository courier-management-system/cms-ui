import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FilterService } from 'primeng/api';
// @ts-ignore
import { DateTime } from 'luxon';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnChanges {

  readonly dateRangeFilter = 'date-range-filter';
  rangeDates: Date[] = [];
  @Input() cols: any[];
  @Input() datas: any[];
  @Input() title: string = '';
  @Output() tableData = new EventEmitter<any[]>();

  constructor(private filterService: FilterService) { }

  ngOnInit(): void {
    this.filterService.register(this.dateRangeFilter, (value: any, filterValue: any): boolean => {      
      let [startDate, endDate] = filterValue.split('-');
      startDate = DateTime.fromString(startDate, 'dd/MM/yyyy').toMillis();
      if (endDate) {
        endDate = DateTime.fromString(endDate, 'dd/MM/yyyy').toMillis();
      }
      let fieldDataMillis = DateTime.fromString(value, 'dd-MM-yyyy').toMillis();
      if (startDate && endDate) {
        return fieldDataMillis >= startDate && fieldDataMillis <= endDate;
      }
      return fieldDataMillis >= startDate;
    });
    this.tableData.emit(this.datas);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.tableData.emit(this.datas);
  }

  filterData(table: Table) {
    let startDate = DateTime.fromJSDate(new Date(this.rangeDates[0])).toFormat('dd/MM/yyyy');
    let endDate = '';
    if (this.rangeDates[1]) {
      endDate = DateTime.fromJSDate(new Date(this.rangeDates[1])).toFormat('dd/MM/yyyy');
    }
    let date = endDate ? `${startDate}-${endDate}` : startDate;
    table.filter(date, 'date', 'date-range-filter');
    setTimeout(() => this.tableData.emit(table.processedData), 1000);
  }

  clearFilter(table: Table) {
    table.reset();
    this.tableData.emit(table.processedData);
  }

}
