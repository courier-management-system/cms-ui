import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoryRoutingModule } from './history-routing.module';
import { ClientHistoryComponent } from './components/client-history/client-history.component';
import { TableComponent } from './components/table/table.component';
import { CardModule } from 'primeng/card';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { AgentHistoryComponent } from './components/agent-history/agent-history.component';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { InputTextModule } from 'primeng/inputtext';


@NgModule({
  declarations: [
    ClientHistoryComponent,
    TableComponent,
    AgentHistoryComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HistoryRoutingModule,
    CardModule,
    TableModule,
    ButtonModule,
    CalendarModule,
    OverlayPanelModule,
    InputTextModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HistoryModule { }
