import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgentHistoryComponent } from './components/agent-history/agent-history.component';
import { ClientHistoryComponent } from './components/client-history/client-history.component';

const routes: Routes = [
  { path: 'client/:id', component: ClientHistoryComponent },
  { path: 'agent/:id', component: AgentHistoryComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule { }
