import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-table-menu',
  templateUrl: './table-menu.component.html',
  styleUrls: ['./table-menu.component.scss']
})
export class TableMenuComponent implements OnInit {

  @Input() menus: any[] = [];
  @Input() data: any;
  @Output() selectMenu = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  onClick(menu: any) {
    this.selectMenu.emit({ action: menu.id, data: this.data });
  }


}
