import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { TableMenuComponent } from './table-menu.component';


@NgModule({
    declarations: [
        TableMenuComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [TableMenuComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TableMenuModule { }
