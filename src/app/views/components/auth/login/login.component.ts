import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { AuthService } from 'src/app/services/auth.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [`
        :host ::ng-deep .p-password input {
            width: 100%;
            padding:1rem;
        }

        :host ::ng-deep .pi-eye{
            transform:scale(1.6);
            margin-right: 1rem;
            color: var(--primary-color) !important;
        }

        :host ::ng-deep .pi-eye-slash{
            transform:scale(1.6);
            margin-right: 1rem;
            color: var(--primary-color) !important;
        }
    `]
})
export class LoginComponent {
  isLoggedIn = false;
  isLoginFailed = false;
  form: FormGroup;
  emailError = '';

  constructor(public layoutService: LayoutService,
    private readonly fb: FormBuilder,
    private messageService: MessageService,
    private router: Router,
    private tokenStorage: TokenStorageService,
    private auth: AuthService) {
    this.form = fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });

    this.onValueChange();
  }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
    }
  }

  onValueChange() {
    this.form.controls['email'].valueChanges.subscribe(() => {
      let error: any = this.form.controls['email'].errors;
      if (error) {
        this.emailError = error.email ? 'Email is not valid' : '';
      } else {
        this.emailError = '';
      }
    })
  }

  login() {
    if (this.form.valid) {
      let formValues = this.form.getRawValue();
      this.auth.login(formValues.email, formValues.password).subscribe({
        next: data => {
          this.tokenStorage.saveToken(data.accessToken);
          this.tokenStorage.saveUser(data);
          this.isLoginFailed = false;
          this.isLoggedIn = true;
          this.router.navigate(['/dashboard'])
        },
        error: (err: any) => {
          if (err.error.statusCode === 404) {
            this.messageService.add({ severity: 'error', summary: 'Error', detail: err.error.message, life: 3000 });
          } else if (err.error.statusCode === 401) {
            this.messageService.add({ severity: 'error', summary: 'Error', detail: err.error.message, life: 3000 });
          } else {
            this.messageService.add({ severity: 'error', summary: 'System Error', detail: 'Something went wrong', life: 3000 });
          }
          this.isLoginFailed = true;
        }
      });
    }
  }
}
