import { Component, Input, OnInit } from '@angular/core';
import { Table } from 'primeng/table';
// @ts-ignore
import { DateTime } from 'luxon';
import { FilterService } from 'primeng/api';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'

@Component({
  selector: 'app-ledger-table',
  templateUrl: './ledger-table.component.html',
  styleUrls: ['./ledger-table.component.scss']
})
export class LedgerTableComponent implements OnInit {

  readonly dateRangeFilter = 'date-range-filter'
  @Input() cols: any[];
  @Input() datas: any[];
  @Input() footer: boolean = false;

  rangeDates: Date[] = [];

  searchValue = '';

  constructor(private filterService: FilterService) { }

  ngOnInit(): void {
    this.filterService.register(this.dateRangeFilter, (value: any, filterValue: any): boolean => {
      let [startDate, endDate] = filterValue.split('-');
      startDate = DateTime.fromString(startDate, 'dd/MM/yyyy').toMillis();
      if (endDate) {
        endDate = DateTime.fromString(endDate, 'dd/MM/yyyy').toMillis();
      }
      let fieldDataMillis = DateTime.fromString(value, 'dd-MM-yyyy').toMillis();
      if (startDate && endDate) {
        return fieldDataMillis >= startDate && fieldDataMillis <= endDate;
      }
      return fieldDataMillis >= startDate;
    });
  }

  filterData(table: Table) {
    let startDate = DateTime.fromJSDate(new Date(this.rangeDates[0])).toFormat('dd/MM/yyyy');
    let endDate = '';
    if (this.rangeDates[1]) {
      endDate = DateTime.fromJSDate(new Date(this.rangeDates[1])).toFormat('dd/MM/yyyy');
    }
    let date = endDate ? `${startDate}-${endDate}` : startDate;
    table.filter(date, 'date', 'date-range-filter');
  }

  clearFilter(table: Table) {
    table.reset();
  }

  getTotal(colName: string, table: Table) {
    let total = 0;
    table.processedData.forEach(data => {
      total += data[colName];
    });
    return total > 0 ? total : '';
  }

  exportPdf(table: Table) {
    const doc = new jsPDF('l', 'mm', 'a4');
    const head = [["#", ...this.cols.map(col => col.header)]];
    let data: any[] = [];
    table.processedData.forEach((item, index) => data.push([index + 1, ...Object.values(item)]));
    const footerValue: any[] = [''];
    this.cols.forEach(col => {
      if (col.isFooter) {
        footerValue.push(this.getTotal(col.field, table));
      } else {
        footerValue.push('');
      }
    });
    data.push(footerValue);
    autoTable(doc, {
      head: head,
      body: data,
      willDrawCell: (data) => {
        let rows = data.table.body;
        if (data.row.index === rows.length - 1) {
          doc.setFillColor(99, 102, 241);
          doc.setTextColor(255);
        }
        if (data.section === 'head') {
          doc.setFillColor(99, 102, 241);
        }
       },
    });
    doc.save('table.pdf');
  }

}
