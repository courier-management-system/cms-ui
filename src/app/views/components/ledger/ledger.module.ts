import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LedgerRoutingModule } from './ledger-routing.module';
import { LedgerTableComponent } from './ledger-table/ledger-table.component';
import { ManageLedgerComponent } from './manage-ledger/manage-ledger.component';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

@NgModule({
  declarations: [
    LedgerTableComponent,
    ManageLedgerComponent
  ],
  imports: [
    CommonModule,
    LedgerRoutingModule,
    ToastModule,
    TableModule,
    TabViewModule,
    ButtonModule,
    FormsModule,
    InputTextModule,
    CalendarModule,
    OverlayPanelModule,
    ProgressSpinnerModule
  ],
  providers: [MessageService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LedgerModule { }
