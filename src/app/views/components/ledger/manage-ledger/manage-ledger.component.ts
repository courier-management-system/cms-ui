import { Component, OnInit } from '@angular/core';
import { LedgerService } from 'src/app/views/service/ledger.service';
// @ts-ignore
import { DateTime } from 'luxon';
import { ExpenseService } from 'src/app/views/service/expense.service';

const DAY_1 = 86400000;

@Component({
  selector: 'app-manage-ledger',
  templateUrl: './manage-ledger.component.html',
  styleUrls: ['./manage-ledger.component.scss']
})
export class ManageLedgerComponent implements OnInit {

  cols: any[] = [];
  paymentCols = [
    { field: 'clientName', header: 'Client Name', filter: { isFilter: true, type: 'text', matchMode: 'contains' }, sort: true },
    { field: 'paidAmount', header: 'Paid Amount', isFooter: true, sort: true },
    { field: 'remark', header: 'Remark', filter: { isFilter: true, type: 'text', matchMode: 'contains' } },
    { field: 'date', header: 'Payment Date', filter: { isFilter: true, type: 'date' }, sort: true }
  ];

  expenseCols = [
    { field: 'expenseType', header: 'Expense Type', filter: { isFilter: true, type: 'text' }, sort: true },
    { field: 'amount', header: 'Amount', isFooter: true, sort: true },
    { field: 'description', header: 'Remark', filter: { isFilter: true, type: 'text', matchMode: 'contains' } },
    { field: 'date', header: 'Date', filter: { isFilter: true, type: 'date' }, sort: true },
  ];

  ocCols = [
    { field: 'date', header: 'Date', filter: { isFilter: true, type: 'date' }, sort: true },
    { field: 'openingBalance', header: 'Opening Balance' },
    { field: 'closingBalance', header: 'Closing Balance' }
  ];
  paymentData: any[] = [];
  expenseData: any[] = [];
  oCData: any[] = [];

  constructor(private readonly ledgerService: LedgerService,
    private readonly expenseService: ExpenseService) {
    this.cols = this.paymentCols;
  }

  async ngOnInit() {
    try {
      let res: any = await this.ledgerService.getLedgerDetails();
      if (res.statusCode === 200) {
        this.createPaymentCompatibleData(res.data);
      }
    } catch (err) {
      console.log('error occure while fetching ledger details', err);

    }
    await this.createExpenseCompatibleData();
    this.createOcCompatibleData();
  }

  handleTabChange(evt: any) {
    if (evt.index === 0) {
      this.cols = this.paymentCols;
    } else if (evt.index === 1) {
      this.cols = this.expenseCols;
    } else if (evt.index === 2) {
      this.cols = this.ocCols;
    }
  }

  createPaymentCompatibleData(data: any) {
    let totalPaidAmount = 0;
    data?.forEach((item: any) => {
      item?.paymentHistory?.forEach((payment: any) => {
        let row = {
          clientName: item.client.companyName, paidAmount: payment.creditAmount,
          remark: payment.remark,
          date: DateTime.fromISO(payment.paymentDate).toFormat('dd-MM-yyyy')
        };
        this.paymentData.push(row);
        totalPaidAmount += payment.creditAmount;
      });
    });
  }

  async createExpenseCompatibleData() {
    try {
      let res: any = await this.expenseService.getExpenses();
      if (res.statusCode === 200) {
        res?.data?.forEach((item: any) => {
          if (!item?.isDeleted) {
            this.expenseData.push({
              expenseType: item.type,
              amount: item.amount,
              description: item.description,
              date: DateTime.fromISO(item.date).toFormat('dd-MM-yyyy')
            });
          }
        });
      }
    } catch (err) {
      console.log('error occured while fetching expense details', err);
    }
  }

  createOcCompatibleData() {
    try {
      let expenses = JSON.parse(JSON.stringify(this.expenseData));
      let payments = JSON.parse(JSON.stringify(this.paymentData));

      let mixedData = [...payments, ...expenses].sort((a: any, b: any) => DateTime.fromString(a.date, 'dd-MM-yyyy').toMillis() - DateTime.fromString(b.date, 'dd-MM-yyyy').toMillis());

      if (mixedData?.length) {
        let startDate = DateTime.fromString(mixedData[0].date, 'dd-MM-yyyy').toMillis();
        let endDate = DateTime.fromJSDate(new Date()).toMillis();
        while (startDate < endDate) {
          let totalExpenseForDay = 0;
          expenses?.forEach((e: any) => {
            if (DateTime.fromString(e.date, 'dd-MM-yyyy').toMillis() === startDate) {
              totalExpenseForDay += e.amount
            }
          });

          let totalPaymentOfDay = 0;
          payments?.forEach((p: any) => {
            if (DateTime.fromString(p.date, 'dd-MM-yyyy').toMillis() === startDate) {
              totalPaymentOfDay += p.paidAmount
            }
          });
          let data;
          if (this.oCData.length) {
            data = {
              date: DateTime.fromMillis(startDate).toFormat('dd-MM-yyyy'),
              openingBalance: this.oCData[this.oCData.length - 1].closingBalance || 0,
              closingBalance: (this.oCData[this.oCData.length - 1].closingBalance + totalPaymentOfDay) - totalExpenseForDay
            }
          } else {
            data = {
              date: DateTime.fromMillis(startDate).toFormat('dd-MM-yyyy'),
              openingBalance: 0,
              closingBalance: totalPaymentOfDay - totalExpenseForDay
            }
          }
          this.oCData.push(data);
          startDate += DAY_1;
        }

      }
    } catch (err) {
      console.log('error occured while generating opening closing data', err);
    }
  }

}
