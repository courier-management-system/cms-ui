import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators, ValidationErrors } from '@angular/forms';
import { MenuItem, MessageService } from 'primeng/api';
import { Client, Train } from 'src/app/views/api/interfaces';
import { ClientService } from 'src/app/views/service/client.service';
import { OrderMasterService } from 'src/app/views/service/order-master.service';
import { TrainService } from 'src/app/views/service/train.service';
// @ts-ignore
import { DateTime } from 'luxon';
import { Table } from 'primeng/table';
import { InvoiceService } from 'src/app/views/service/invoice.service';
import { ActionMode } from 'src/app/helpers/menus';
import { FormErrorMessage } from 'src/app/helpers/validation-utils';


@Component({
  selector: 'app-manage-order',
  templateUrl: './manage-order.component.html',
  styleUrls: ['./manage-order.component.scss']
})
export class ManageOrderComponent implements OnInit {

  ordersDetailCopy: any[] = [];
  orderDialog: boolean = false;
  deleteOrderDialog: boolean = false;
  deleteOrdersDialog: boolean = false;
  submitted: boolean = false;
  items: MenuItem[];
  activeStepIndex = 0;
  courierDate = new Date;
  clients: Client[] = [];
  vehicles: Train[] = [];
  selectedClientId = null;
  selectedTrainId = null;
  orders: any[] = [];
  order: any = {};
  isNextActive = false;
  form: FormGroup;
  actionMenu: any[] = [{ id: 'edit', label: 'Edit' }, { id: 'delete', label: 'Delete' }];
  selectedPaymentType = null;
  viewWholeDetail: boolean = false;
  cols: any[];
  isNext = false;
  isDeletedTab = false;
  selectedClientForPrint: any[] = [];
  enablePrintButton = false;
  invoiceDialog = false;
  printData: any[] = [];
  generateInvoiceIds: any[] = [];
  errors: { [key: string]: FormErrorMessage } = {};

  constructor(private readonly clientService: ClientService,
    private readonly vehicleService: TrainService,
    private readonly orderService: OrderMasterService,
    private messageService: MessageService,
    private fb: FormBuilder,
    private invoiceService: InvoiceService) {

    this.form = fb.group({
      description: new FormControl(''),
      vphNumber: new FormControl('', [Validators.required, Validators.pattern(/^(\w+ ?)*$/)]),
      packetNumber: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]*$/)]),
      weight: new FormControl('', [Validators.required, this.weightValidator()]),
      type: new FormControl('', [Validators.pattern(/^[a-zA-Z ]*$/)]),
      rate: new FormControl('', [Validators.required, this.amountValidator()]),
      totalAmount: new FormControl({ value: 0, disabled: true }),
      paidAmount: new FormControl({ value: 0, disabled: true }),
      balanceAmount: new FormControl({ value: 0, disabled: true }),
      paymentType: new FormControl('', [Validators.pattern(/^(\w+ ?)*$/)])
    }, { updateOn: 'blur' });


    this.items = [
      { label: 'Courier Details' },
      { label: 'Payment' }
    ];
    this.onValuechanges();
  }

  async ngOnInit() {
    let clientRes: any = await this.clientService.getClients();
    if (clientRes.statusCode === 200) {
      this.clients = clientRes.data?.filter((client: any) => !client.isDeleted);;
    }

    let trainRes: any = await this.vehicleService.getTrains();
    if (trainRes.statusCode === 200) {
      this.vehicles = trainRes.data.filter((train: any) => !train.isDeleted);;
    }

    this.getGeneratedInvoiceIds();
    this.fetchData();

    this.cols = [
      { field: 'vphNumber', header: 'VPH Number' },
      { field: 'clientName', header: 'Client Name' },
      { field: 'trainNumber', header: 'Train Number' },
      { field: 'packetNumber', header: 'Packet Number' },
      { field: 'weight', header: 'Weight' },
      { field: 'rate', header: 'Rate' },
      { field: 'totalAmount', header: 'Total Amount' },
      { field: 'paidAmount', header: 'Paid Amount' },
      { field: 'balanceAmount', header: 'Balance Amount' },
    ];
  }

  async getGeneratedInvoiceIds() {
    let invoice: any = await this.invoiceService.getInvoices();
    if (invoice.data?.length) {
      invoice.data.forEach((item: any) => {
        item.invoiceItems.forEach((i: any) => {
          this.generateInvoiceIds.push(i.vphNumber)
        });
      });
    }
  }

  onValuechanges() {
    Object.keys(this.form.controls).forEach((key: string) => {
      this.form.get(key)?.valueChanges.subscribe(() => {
        if (key === 'weight' || key === 'rate') {
          this.calculateTotalAmount();
        } else if (key === 'paidAmount') {
          this.calculateBalanceamount();
        } else if (key === 'totalAmount') {
          if (this.form.controls['totalAmount'].value <= 0) {
            this.form.controls['paidAmount'].disable();
          } else {
            this.form.controls['paidAmount'].enable();
          }
        }

        delete this.errors[key];
        const controlErrors: any = this.form.get(key)?.errors || null;
        if (controlErrors !== null) {
          if (key === 'vphNumber' && controlErrors?.pattern) {
            this.errors[key] = { key, arg: 'Invalid VPH number' };
          } else if (key === 'paymentType' && controlErrors?.pattern) {
            this.errors[key] = { key, arg: 'Invalid payment type' };
          } else if (key === 'packetNumber' && controlErrors?.pattern) {
            this.errors[key] = { key, arg: 'Invalid packet number' };
          } else if (key === 'type' && controlErrors?.pattern) {
            this.errors[key] = { key, arg: 'Type should be contains alphabets only' };
          } else if (key === 'weight' && controlErrors?.invalid) {
            this.errors[key] = { key, arg: 'Weight cannot be less than zero' };
          } else if (key === 'rate' && controlErrors?.invalid) {
            this.errors[key] = { key, arg: 'Rate cannot be less than zero' };
          }
        }
      });
    });
  }

  checkIsNextStep() {
    let frmCtrl: any = this.form.controls;
    return (frmCtrl.vphNumber.value && frmCtrl.weight.value && frmCtrl.rate.value
      && frmCtrl.packetNumber.value && this.selectedClientId && this.selectedTrainId);
  }

  async fetchData() {
    try {
      let result: any = await this.orderService.getOrders();
      if (result?.statusCode === 200) {
        this.ordersDetailCopy = result?.data;
        this.filterOrder();
      }
    } catch (error) {
      console.log('error occured while fetching order', error);
    }
  }

  filterOrder() {
    if (this.ordersDetailCopy.length) {
      if (this.isDeletedTab) {
        this.orders = this.ordersDetailCopy?.filter((order: any) => order.isDeleted);
      } else {
        this.orders = this.ordersDetailCopy?.filter((order: any) => !order.isDeleted);
      }
    }
  }

  openNew() {
    this.submitted = false;
    this.orderDialog = true;
  }

  calculateTotalAmount() {
    let frmCtrl: any = this.form.controls;
    if (!frmCtrl.weight.error && !frmCtrl.rate.error && frmCtrl.weight.value && frmCtrl.rate.value) {
      let weight = Number(frmCtrl.weight.value.split(' ')[0]);
      frmCtrl.totalAmount.setValue(weight * frmCtrl.rate.value);
    } else {
      frmCtrl.totalAmount.setValue(0);
    }
  }

  calculateBalanceamount() {
    let frmCtrl: any = this.form.controls;
    if (frmCtrl.totalAmount.value > 0) {
      frmCtrl.balanceAmount.setValue(frmCtrl.totalAmount.value - frmCtrl.paidAmount.value);
    }
  }

  hideDialog() {
    this.activeStepIndex = 0;
    this.orderDialog = false;
    this.submitted = false;
    this.selectedClientId = null;
    this.selectedTrainId = null;
    this.order = {};
    this.form.reset();
    this.form.controls['vphNumber'].enable();
    this.form.controls['rate'].enable();
    this.form.controls['weight'].enable();
    this.form.controls['paidAmount'].enable();
  }

  prevStep() {
    if (this.activeStepIndex >= 0) {
      this.activeStepIndex--;
    }
  }

  nextStep() {
    if (this.activeStepIndex < 1) {
      this.activeStepIndex++;
    }
  }

  deleteOrder(order: any) {
    this.deleteOrderDialog = true;
    this.order = { ...order };
  }

  async confirmDelete() {
    this.deleteOrderDialog = false;
    try {
      let courierId = this.order?.id;
      if (courierId) {
        await this.orderService.deleteOrder(courierId);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Order Deleted', life: 3000 });
        this.fetchData();
      }
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'System error occured', life: 3000 });
      console.log('error occurred while delete client', error);
    }
    this.order = {};
  }

  editOrder(order: any) {
    this.order = { ...order };
    let frmCtl: any = this.form.controls;
    frmCtl.description.setValue(order?.description);
    frmCtl.vphNumber.setValue(order?.vphNumber);
    frmCtl.packetNumber.setValue(order?.packetNumber);
    frmCtl.weight.setValue(`${order?.weight} ${order?.weightType}`);
    frmCtl.rate.setValue(order?.rate);
    frmCtl.totalAmount.setValue(order?.totalAmount);
    frmCtl.paidAmount.setValue(order?.paidAmount);
    frmCtl.balanceAmount.setValue(order?.balanceAmount);
    frmCtl.type.setValue(order?.type);
    frmCtl.paymentType.setValue(order?.paymentType);
    frmCtl.vphNumber.disable();
    frmCtl.rate.disable();
    frmCtl.weight.disable();
    frmCtl.paidAmount.disable();

    this.selectedClientId = order?.clientId;
    this.selectedTrainId = order?.trainId;
    this.courierDate = new Date(DateTime.fromISO(order?.courierDate));
    this.orderDialog = true;
  }

  handleTabChange(evt: any) {
    var index = evt.index;
    this.isDeletedTab = index === 1;
    if (this.isDeletedTab) {
      this.actionMenu = [{ id: 'restore', label: 'Restore' }];
    } else {
      this.actionMenu = [{ id: 'edit', label: 'Edit' }, { id: 'delete', label: 'Delete' }];
    }
    this.filterOrder();
  }

  async saveOrder() {
    if (this.form.valid) {
      let payload = this.generatePayload();
      if (this.order?.id) {
        try {
          await this.orderService.updateOrder(payload, this.order?.id);
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'client Updated', life: 3000 });
          this.fetchData();
          this.hideDialog();
        } catch (error) {
          console.log('Error occured while saving user details', error);
          this.messageService.add({ severity: 'error', summary: 'Unsuccessful', detail: 'System error occured', life: 3000 });
        }
      } else {
        try {
          await this.orderService.addOrder(payload);
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Courier Added', life: 3000 });
          this.fetchData();
          this.hideDialog();
        } catch (error) {
          console.log('Error occured while saving courier details', error);
          this.messageService.add({ severity: 'error', summary: 'Unsuccessful', detail: 'System error occured', life: 3000 });
        }
      }
    }
  }

  generatePayload() {
    let formValues = this.form.getRawValue();
    let [weight, weightType] = formValues['weight'].split(' ');
    if (weight && weightType) {
      formValues['weight'] = weight;
      formValues['weightType'] = weightType;
    } else if (weight && !weightType) {
      formValues['weight'] = weight;
      formValues['weightType'] = 'Kg';
    }

    return {
      ...formValues,
      courierDate: DateTime.fromJSDate(this.courierDate).toFormat('yyyy-MM-dd'),
      clientId: this.selectedClientId,
      trainId: this.selectedTrainId,
    }
  }

  async restoreOrder(order: any) {
    try {
      await this.orderService.restoreOrder(order.id);
      this.fetchData();
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'client restored', life: 3000 });
    } catch (e) {
      console.log('error occured while restoring client details', e);

    }
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }

  selectPrintRow() {
    let clientName = this.selectedClientForPrint[0]?.client?.companyName;
    this.enablePrintButton = this.selectedClientForPrint?.every(c => c?.client?.companyName === clientName)
      && this.selectedClientForPrint?.length > 0;
    this.printData = this.selectedClientForPrint.map(inv => {
      return {
        vphNumber: inv.vphNumber,
        consignor: inv.client?.companyName,
        fromCity: inv.train?.fromStation,
        toCity: inv.train?.toStation,
        weight: `${inv.weight} ${inv.weightType || ''}`,
        rate: inv.rate,
        courierDate: inv.courierDate,
        amount: inv.totalAmount,
        description: inv.description,
        customerGst: inv.client?.gstNo
      }
    });
  }

  hideInvoiceDialog() {
    this.invoiceDialog = false;
    this.enablePrintButton = false;
    this.getGeneratedInvoiceIds();
  }

  async printInvoice() {
    this.invoiceDialog = true
  }

  selectActionMenu(evt: any) {
    if (evt.action === ActionMode.EDIT.toString()) {
      this.editOrder(evt.data);
    } else if (evt.action === ActionMode.DELETE.toString()) {
      this.deleteOrder(evt.data);
    } else if (evt.action === ActionMode.RESTORE.toString()) {
      this.restoreOrder(evt.data);
    }
  }

  weightValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = Number(control.value?.split(' ')[0]);
      return value < 0 ? { invalid: true } : null;
    }
  }

  amountValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;
      return value < 0 ? { invalid: true } : null;
    }
  }
}
