import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { ManageOrderComponent } from './manage-order/manage-order.component';
import { StepsModule } from 'primeng/steps';
import { ToolbarModule } from 'primeng/toolbar';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { TableModule } from 'primeng/table';
import { MenuModule } from 'primeng/menu';
import { MatMenuModule } from '@angular/material/menu';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';
import { TabViewModule } from 'primeng/tabview';
import { OrderInvoiceComponent } from './order-invoice/order-invoice.component';
import { TableMenuModule } from '../table-menu/table-menu.module';


@NgModule({
  declarations: [
    ManageOrderComponent,
    OrderInvoiceComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    StepsModule,
    ToolbarModule,
    ToastModule,
    DialogModule,
    ButtonModule,
    DropdownModule,
    CalendarModule,
    InputNumberModule,
    InputTextModule,
    InputTextareaModule,
    FormsModule,
    ReactiveFormsModule,
    ToggleButtonModule,
    TableModule,
    MenuModule,
    RippleModule,
    TabViewModule,
    TableMenuModule
  ],
  exports: [OrderInvoiceComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [MessageService]
})
export class OrdersModule { }
