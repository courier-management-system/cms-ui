import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { MessageService } from 'primeng/api';
import { InvoiceService } from 'src/app/views/service/invoice.service';
// @ts-ignore
import { DateTime } from 'luxon';
import { ClientService } from 'src/app/views/service/client.service';

const INITIAL_INVOICE_NUMBER = 1299;

@Component({
  selector: 'app-order-invoice',
  templateUrl: './order-invoice.component.html',
  styleUrls: ['./order-invoice.component.scss']
})
export class OrderInvoiceComponent implements OnInit, OnChanges {

  @Input() invoiceData: any[] = [];
  @Input() actionType: string;
  @Output() closeDialogEvent = new EventEmitter();
  data: any[] = [];
  invoiceNumber: string = '';
  sgstPercentage = 0;
  cgstPercentage = 0;
  igstPercentage = 0;
  sgstAmount = 0;
  cgstAmount = 0;
  igstAmount = 0;
  totalAmount = 0;
  preGstTotalAmount = 0;
  isGstPresent = false;
  invoiceId: number;

  constructor(private readonly invoiceService: InvoiceService,
    private messageService: MessageService,
    private readonly clientService: ClientService) { }

  ngOnInit() {
    this.calculatePreGstAmount();
  }

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    if (this.actionType === 'edit') {
      console.log(this.invoiceData);
      
      this.invoiceId = this.invoiceData[0].id;
      this.invoiceNumber = this.invoiceData[0].invoiceNumber;
      this.sgstPercentage = this.invoiceData[0].sgstPercentage;
      this.cgstPercentage = this.invoiceData[0].cgstPercentage;
      this.igstPercentage = this.invoiceData[0].igstPercentage;
      this.invoiceData = this.invoiceData[0]?.invoiceItems;
      try {
        let res: any = await this.clientService.getClients();
        this.isGstPresent = res.data?.filter((client: any) => client.companyName === this.invoiceData[0].consignor && client.gstNo).length > 0;
      } catch (err) {
        console.log('error occurred while fetching data', err);
      }
    } else {
      this.invoiceNumber = await this.generateInvoiceNumber() || '';
      this.isGstPresent = this.invoiceData[0]?.customerGst ? true : false;
    }    
    this.calculatePreGstAmount();
  }

  calculatePreGstAmount() {
    if (this.invoiceData) {
      this.preGstTotalAmount = 0;
      this.invoiceData.forEach(inv => {
        this.preGstTotalAmount += inv.amount;
      })
    }
  }

  romoveData(vphNumber: string) {
    this.invoiceData = this.invoiceData.filter(inv => inv.vphNumber !== vphNumber);
  }

  closeDialog() {
    this.closeDialogEvent.emit();
  }

  async save() {
    try {
      let payload = this.generatePayload();
      if (this.actionType === 'create') {
        await this.invoiceService.addInvoice(payload);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Invoice saved', life: 3000 });
      } else if (this.actionType === 'edit' && this.invoiceId) {
        await this.invoiceService.updateInvoice(payload, this.invoiceId);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Invoice updated', life: 3000 });
      }
      this.closeDialog();
    } catch (err) {
      console.log('error occured while saving invoice data', err);
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'System error occured', life: 3000 });
    }
  }

  generatePayload() {
    if (this.invoiceData.length) {
      let invoiceItems = this.invoiceData.map(inv => {
        return {
          vphNumber: inv.vphNumber,
          consignor: inv.consignor,
          fromCity: inv.fromCity,
          toCity: inv.toCity,
          weight: `${inv.weight} ${inv.weightType || ''}`,
          rate: inv.rate,
          courierDate: inv.courierDate,
          amount: inv.amount,
          description: inv.description,
        }
      });
      return {
        invoiceNumber: this.invoiceNumber,
        customerName: this.invoiceData[0].consignor,
        customerGst: this.invoiceData[0].customerGst,
        address: this.invoiceData[0].address || '',
        state: this.invoiceData[0].state || '',
        sgstPercentage: this.sgstPercentage || 0,
        sgst: (this.sgstPercentage / 100 * this.preGstTotalAmount).toFixed(2),
        cgstPercentage: this.cgstPercentage || 0,
        cgst: (this.cgstPercentage / 100 * this.preGstTotalAmount).toFixed(2),
        igstPercentage: this.igstPercentage || 0,
        igst: (this.igstPercentage / 100 * this.preGstTotalAmount).toFixed(2),
        totalAmount: this.getTotalAmount(),
        invoiceItems: invoiceItems
      }
    }
    return null;
  }

  getTotalAmount() {
    let totalAmount = this.preGstTotalAmount;
    if (this.sgstPercentage > 0) {
      totalAmount += this.sgstPercentage / 100 * this.preGstTotalAmount;
    }
    if (this.cgstPercentage > 0) {
      totalAmount += this.cgstPercentage / 100 * this.preGstTotalAmount;
    }
    if (this.igstPercentage > 0) {
      totalAmount += this.igstPercentage / 100 * this.preGstTotalAmount;
    }
    return totalAmount.toFixed(2);
  }

  async generateInvoiceNumber() {
    try {
      let invoice: any = await this.invoiceService.getLastInvoices();
      if (invoice.statusCode === 200) {
        let endIndex = invoice.data?.invoiceNumber.length - 8;
        let startIndex = this.invoiceData[0].fromCity.length + 4;
        let lastInvoiceNumer = invoice.data ? Number(invoice.data?.invoiceNumber.slice(startIndex, endIndex)) : INITIAL_INVOICE_NUMBER;
        let clientName = this.invoiceData[0].consignor.replace(' ', '').slice(0, 4);
        let todayDate = DateTime.fromJSDate(new Date()).toFormat('dd/MM/yyyy').replaceAll('/', '');
        return `${this.invoiceData[0].fromCity}${clientName}${lastInvoiceNumer + 1}${todayDate}`.toUpperCase();
      }
      return null;
    } catch (err) {
      console.log('error occured while generating invoice number', err);
      return null;
    }
  }


}
