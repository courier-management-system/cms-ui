import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { ActionMode } from 'src/app/helpers/menus';
import { FormErrorMessage } from 'src/app/helpers/validation-utils';
import { Client } from 'src/app/views/api/interfaces';
import { ClientService } from 'src/app/views/service/client.service';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})
export class CrudComponent implements OnInit {

  clientDialog: boolean = false;
  clientsDetailCopy: Client[] = [];
  deleteClientDialog: boolean = false;
  deleteClientsDialog: boolean = false;
  clients: Client[] = [];
  client: Client = {};
  selectedclients: Client[] = [];
  submitted: boolean = false;
  cols: any[] = [];
  stateOptions: any[];
  isDeletedTab = false;
  actionMenu: any[] = [{ id: 'edit', label: 'Edit' }, { id: 'delete', label: 'Delete' },
  { id: 'history', label: 'Client' }, { id: 'agentHistory', label: 'Agent' }];
  rowsPerPageOptions = [5, 10, 20];
  form: FormGroup;
  errors: { [key: string]: FormErrorMessage } = {};

  constructor(private clientService: ClientService, private messageService: MessageService,
    private fb: FormBuilder, private router: Router) {
    this.form = fb.group({
      agentName: new FormControl('', [Validators.required, this.duplicateValidator()]),
      companyName: new FormControl('', [Validators.required, this.companyNameValidator()]),
      email: new FormControl('', [Validators.email]),
      contact: new FormControl('', [Validators.pattern(/^[6-9]{1}[0-9]{9}$/)]),
      address: new FormControl(''),
      city: new FormControl(''),
      pincode: new FormControl('', [Validators.pattern(/^[1-9][0-9]{5}$/)]),
      state: new FormControl(''),
      gstNo: new FormControl('', [Validators.pattern(/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/)]),
    }, { updateOn: 'blur' });
    this.collectErrorOnValueChanges();
    this.stateOptions = [{ label: 'Clients', value: 'clients' }, { label: 'Deleted', value: 'deleted' }];

  }

  async ngOnInit() {
    this.fetchData();
    this.cols = [
      { field: 'companyName', header: 'Company Name', sorting: true },
      { field: 'agentName', header: 'Name', sorting: true },
      { field: 'email', header: 'Email', sorting: true },
      { field: 'contact', header: 'Contact', sorting: true },
      { field: 'city', header: 'City', sorting: true },
      { field: 'state', header: 'State', sorting: true },
      { field: 'pincode', header: 'Pincode', sorting: true },
      { field: 'gstNo', header: 'GST', sorting: true },
      { field: 'action', header: '', sorting: false },
    ];
  }

  async fetchData() {
    try {
      let result: any = await this.clientService.getClients();
      if (result?.statusCode === 200) {
        this.clientsDetailCopy = result?.data;
        this.filterClient();
      }
    } catch (error) {
      console.log('error occured while fetching client', error);
    }
  }

  filterClient() {
    if (this.clientsDetailCopy.length) {
      if (this.isDeletedTab) {
        this.clients = this.clientsDetailCopy?.filter((client: any) => client.isDeleted);
      } else {
        this.clients = this.clientsDetailCopy?.filter((client: any) => !client.isDeleted);
      }
    }
  }

  collectErrorOnValueChanges() {
    Object.keys(this.form.controls).forEach((key: string) => {
      this.form.get(key)?.valueChanges.subscribe(() => {
        delete this.errors[key];
        const controlErrors: any = this.form.get(key)?.errors || null;
        if (controlErrors !== null) {
          if (key === 'companyName' && controlErrors?.minlength) {
            this.errors[key] = { key, arg: 'Company name must be at least 4 characters long' };
          } else if (key === 'email' && controlErrors?.email) {
            this.errors[key] = { key, arg: 'Email is not valid' };
          } else if (key === 'agentName' && controlErrors.duplicate) {
            this.errors[key] = { key, arg: 'Name already exist' };
          } else if (key === 'contact' && controlErrors.pattern) {
            this.errors[key] = { key, arg: 'Invalid contact number' };
          } else if (key === 'pincode' && controlErrors.pattern) {
            this.errors[key] = { key, arg: 'Invalid pincode' };
          } else if (key === 'gstNo' && controlErrors.pattern) {
            this.errors[key] = { key, arg: 'Invalid gst number' };
          }
        }
      });
    });
  }

  openNew() {
    this.client = {};
    this.submitted = false;
    this.clientDialog = true;
  }

  deleteSelectedclients() {
    this.deleteClientsDialog = true;
  }

  editClient(client: Client) {
    this.client = { ...client };
    let frmCtl: any = this.form.controls;
    frmCtl.agentName.clearValidators();
    frmCtl.agentName.updateValueAndValidity();
    frmCtl.agentName.setValue(client.agentName);
    frmCtl.companyName.setValue(client.companyName);
    frmCtl.email.setValue(client.email);
    frmCtl.contact.setValue(client.contact);
    frmCtl.address.setValue(client.address);
    frmCtl.city.setValue(client.city);
    frmCtl.state.setValue(client.state);
    frmCtl.pincode.setValue(client.pincode);
    frmCtl.gstNo.setValue(client.gstNo);
    frmCtl.companyName.disable();
    frmCtl.agentName.disable();
    this.clientDialog = true;
  }

  deleteClient(client: Client) {
    this.deleteClientDialog = true;
    this.client = { ...client };
  }

  confirmDeleteSelected() {
    this.deleteClientsDialog = false;
    this.clients = this.clients.filter(val => !this.selectedclients.includes(val));
    this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'clients Deleted', life: 3000 });
    this.selectedclients = [];
  }

  async confirmDelete() {
    this.deleteClientDialog = false;
    try {
      let clientId = this.client['id'];
      if (clientId) {
        await this.clientService.deleteClient(clientId);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'client Deleted', life: 3000 });
        this.fetchData();
      }
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'System error occured', life: 3000 });
      console.log('error occurred while delete client', error);
    }
    this.client = {};
  }

  hideDialog() {
    this.clientDialog = false;
    this.submitted = false;
    this.form.reset();
    this.form.controls['companyName'].enable();
    this.form.controls['agentName'].enable();
    this.errors = {};
  }

  handleTabChange(evt: any) {
    var index = evt.index;
    this.isDeletedTab = index === 1;
    if (this.isDeletedTab) {
      this.actionMenu = [{ id: 'restore', label: 'Restore' }];
    } else {
      this.actionMenu = [{ id: 'edit', label: 'Edit' }, { id: 'delete', label: 'Delete' }, { id: 'history', label: 'History' }];
    }
    this.filterClient();
  }

  async saveClient() {
    this.submitted = true;
    if (this.form.valid) {
      let payload = this.form.getRawValue();
      if (this.client.id) {
        try {
          await this.clientService.updateClient(payload, this.client.id);
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'client Updated', life: 3000 });
          this.fetchData();
          this.client = {};
          this.hideDialog();
        } catch (error) {
          console.log('Error occured while saving user details', error);
          this.messageService.add({ severity: 'error', summary: 'Unsuccessful', detail: 'System error occured', life: 3000 });
        }
      } else {
        try {
          await this.clientService.addClient(payload);
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'client Added', life: 3000 });
          this.fetchData();
          this.client = {};
          this.hideDialog();
        } catch (error) {
          console.log('Error occured while saving user details', error);
          this.messageService.add({ severity: 'error', summary: 'Unsuccessful', detail: 'System error occured', life: 3000 });
        }
      }
    }
  }

  async restoreClient(client: any) {
    try {
      await this.clientService.restoreClient(client.id);
      this.fetchData();
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'client restored', life: 3000 });
    } catch (e) {
      console.log('error occured while restoring client details', e);
    }
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }

  selectActionMenu(evt: any) {
    if (evt.action === ActionMode.EDIT.toString()) {
      this.editClient(evt.data);
    } else if (evt.action === ActionMode.DELETE.toString()) {
      this.deleteClient(evt.data);
    } else if (evt.action === ActionMode.RESTORE.toString()) {
      this.restoreClient(evt.data);
    } else if (evt.action === ActionMode.HISTORY.toString()) {
      window.open(`/#/history/client/${evt.data?.id}`)
    } else if (evt.action === ActionMode.AGENT_HISTORY.toString()) {
      window.open(`/#/history/agent/${evt.data?.agentName}`)
    }
  }

  duplicateValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;
      let agent = this.clients?.find((client: any) => client.agentName?.toLowerCase() === value?.toLowerCase());
      return agent ? { duplicate: true } : null;
    }
  }

  companyNameValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value?.replaceAll(' ', '');
      return value?.length < 4 ? { minlength: true } : null;
    }
  }
}

