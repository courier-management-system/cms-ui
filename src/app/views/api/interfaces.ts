export interface Client {
    id?: number,
    agentName?: string,
    companyName?: string,
    email?: string,
    contact?: string,
    address?: string,
    city?: string,
    state?: string,
    pincode?: number,
    gstNo?: string
}

export interface Train {
    id?: number,
    trainNumber?: string,
    toStation?: string,
    fromStation?: number
}
